# ------------------------------------------------------------------------------
# This file defines a target named ClangTidy that run clang-tidy on every source
# file of the current source directory that differ from its homonymous in the
# origin branch
#
# Two cache variables are used here: - CLANG_TIDY_SOURCES_LIST: defines the path
# to a temporary file that will be filled by the GitInfos.cmake script with the
# path of every source file to analyze (by default /tmp/.diff_list.txt) -
# CLANG_TIDY_FILE_EXTENSION: is a comma separated list of extension that
# identify sources list (by default .cc, .cxx, .h)
# ------------------------------------------------------------------------------

# Prerequisites checking
find_program(CLANG_TIDY_PATH clang-tidy)
if(NOT CLANG_TIDY_PATH)
  message(FATAL_ERROR "clang-tidy not found! Aborting...")
endif()

# Cache variables definition
set(CLANG_TIDY_SOURCES_LIST
    "/tmp/.diff_list.txt"
    CACHE STRING
          "Temporary file where the output of the git diff command is stored")
set(CLANG_TIDY_FILE_EXTENSION
    ".cc,.cxx,.h"
    CACHE
      STRING
      "List of comma separated extensions that the name of the file to analyze should match"
)

# This function defines the target ClangTidy It executes the GitInfos.cmake
# script and as calls clang-tidy as post build command
function(clang_tidy_diff)
  add_custom_target(
    ClangTidy
    COMMAND
      ${CMAKE_COMMAND} -DGIT_DIFF_DESTINATION=${CLANG_TIDY_SOURCES_LIST}
      -DFILE_EXTENSION=${CLANG_TIDY_FILE_EXTENSION} -P
      ${PROJECT_SOURCE_DIR}/cmake/GitInfos.cmake
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    COMMENT "Retrieving Git informations")

  add_custom_command(
    TARGET ClangTidy
    POST_BUILD
    COMMAND
      bash -c
      "if [[ -f ${CLANG_TIDY_SOURCES_LIST} ]]; then cat ${CLANG_TIDY_SOURCES_LIST} | xargs ${CLANG_TIDY_PATH} --config-file=${PROJECT_SOURCE_DIR}/.clang-tidy -p=${CMAKE_BINARY_DIR}; fi"
    VERBATIM
    COMMENT "Running clang-tidy on source files that differ from origin branch")
endfunction()

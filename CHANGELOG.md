# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

Guiding Principles:

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

Types of changes:

- **Added** for new features.
- **Changed** for changes in existing functionality.
- **Deprecated** for soon-to-be removed features.
- **Removed** for now removed features.
- **Fixed** for any bug fixes.
- **Security** in case of vulnerabilities.

## Unreleased

### Added

### Fixed

## Version 1.0.2 - 2023-02-20

### Added

- Introduces storage settings for uses in the assistant

- In case the FillMaterial option is on, the output meshes have the same type
  as the input meshes

  Closes #27

### Fixed

- Fixes the way the types of the cells produced by the clip method are attributed
  
  Closes #25
  Closes #26

- Fixes the wrong values of the cell fields after applying the vtkMaterialInterface filter.
  
  Closes #28

- Reduces the amount of log messages.

  Closes #24

- Adds property settings to set the regex used for renaming a source in the pipeline

- Avoid holes in the final mesh (Emmental aspect) if the current PV server
  does not have any interface arrays.

  Closes #22

- Avoid SIGSEGV crashes when interface data are ill formed. Also checks the
  validity of the order array.

  Closes #23

## Version 1.0.1 - 2022-12-19

### Fixed

- Fixes the writer to dat files. This writer allows contour extraction on

  - MultiBlockDataSet
  - PolyData
  - UnstructuredGrid

  and writes the obtained contour into a file with `.dat` extension.

  To use it, just click on `File` -> `Save Data...` then select the field
  `CEA Writer DAT (Python)(*.dat)` in the `Files of type:` selection box.

  Closes #17

## Version 1.0.0 - 2022-11-25

### Fixed

- Fixes the transfer of the Field data through the vtkMaterialInterface filter.
  Closes #14

- Fixes the missing interface for some materials problem.
  The topology construction of the last material of the mixed cell was failing.
  Closes #12

## Version 0.3.3 - 2022-07-18

image: $CI_REGISTRY/themys_private/themysdocker:latest

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

# Every stage will be run one after the other and if the previous stage finishes without error
# In each stage the jobs are run in //
stages:
  - build
  - tests
  - checks
  - install

build_debug_legacy:
  stage: build
  before_script:
    - apt-get update
    - apt-get install -y gcc-10 g++-10
  script:
    - mkdir build_themysserverplugins_debug_cpp10
    - cd build_themysserverplugins_debug_cpp10
    - CXX=/usr/bin/g++-10 CC=/usr/bin/gcc-10 cmake -GNinja  -DCMAKE_BUILD_TYPE=Debug ..
    - cmake --build .
  tags:
    - docker

# This job build the themysserverplugins project in debug mode.
# It depends on the fetch_themysserverplugins job to get access to the sources
# It defines the build_themysserverplugins_debug directory as an artifact so that the tests can use the binary producted.
build_debug:
  stage: build
  script:
    - mkdir build_themysserverplugins_debug
    - cd build_themysserverplugins_debug
    - cmake -GNinja  -DCMAKE_BUILD_TYPE=Debug -DENABLE_CODE_COVERAGE=ON ..
    - cmake --build .
  tags:
    - docker
  artifacts:
    paths:
      - build_themysserverplugins_debug
    expire_in: 2 days

# This job build the themysserverplugins project in release mode.
# It depends on the fetch_themysserverplugins job to get access to the sources
# It defines the build_themysserverplugins_release directory as an artifact so that the documentation can be generated
build_release:
  stage: build
  script:
    - mkdir build_themysserverplugins_release
    - cd build_themysserverplugins_release
    - cmake -GNinja -DCMAKE_BUILD_TYPE=Release ..
    - cmake --build .
  tags:
    - docker
  artifacts:
    paths:
      - build_themysserverplugins_release # Necessary to call doxygen through cmake
    expire_in: 2 days

# This job launches the tests on the debug exe.
# It is launched only for CI triggered by MR or after merging on master.
# It's main interest is to measure code coverage.
# It depends on the fetch_themysserverplugins job to get access to the databases needed by the tests
# It depends on the build_debug job to get access to the binaries tested
all_tests_debug:
  stage: tests
  dependencies:
    - build_debug
  script:
    - cd build_themysserverplugins_debug
    # The coverage target will run the tests and measure code coverage
    - xvfb-run ctest
  after_script:
    - cd build_themysserverplugins_debug
    - gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root ${CI_PROJECT_DIR}
  coverage: /^\s*lines:\s*\d+.\d+\%/
  tags:
    - docker
  artifacts:
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: build_themysserverplugins_debug/coverage.xml

# This job launches the tests on the release exe.
# It depends on the fetch_themysserverplugins job to get access to the databases needed by the tests
# It depends on the build_release job to get access to the binaries tested
all_tests_release:
  stage: tests
  dependencies:
    - build_release
  script:
    - cd build_themysserverplugins_release
    - xvfb-run ctest
  tags:
    - docker
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS

# This job checks if the CHANGELOG.md file has been modified.
# It is run only for a merge request and the job is allowed to failed
check_changelog:
  stage: checks
  allow_failure: true
  before_script:
    - apt-get update
    - apt-get install -y git-all
  script:
    - echo "CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH}"
    - FORK_COMMIT=$(git merge-base main $CI_COMMIT_BRANCH)
    - echo "FORK_COMMIT=${FORK_COMMIT}"
    - git diff-tree --no-commit-id --name-only -r $FORK_COMMIT -r $CI_COMMIT_SHA|grep CHANGELOG.md
  tags:
    - docker
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS

# This job install the themysserverplugins and launch a test to ensure the
# installation is ok.
# The tests are located in the build tree because they are not installed.
# However the install phase is truly tested because the PV_PLUGIN_PATH points
# toward the install tree, thus the plugin used are those that have been installed
# It depends on the fetch_themysserverplugins job to get access to the databases needed by the tests
# It depends on the build_release job to get access to the binaries tested
test_install:
  stage: install
  dependencies:
    - build_release
  variables:
    PARAVIEW_DATA_ROOT: "${CI_BUILDS_DIR}/${CI_PROJECT_PATH}"
    THEMYSSERVERPLUGINS_INSTALL_PATH: "${CI_BUILDS_DIR}/${CI_PROJECT_PATH}/themysserverplugins_install"
    PV_PLUGIN_PATH: "${THEMYSSERVERPLUGINS_INSTALL_PATH}/lib/PluginsPython"
    PYTHONPATH: "${CI_BUILDS_DIR}/${CI_PROJECT_PATH}/build_themysserverplugins_release/ThemysFilters/Testing/WithParaView"
  before_script:
    - echo "PARAVIEW_DATA_ROOT= ${PARAVIEW_DATA_ROOT}"
    - echo "THEMYSSERVERPLUGINS_INSTALL_PATH= ${THEMYSSERVERPLUGINS_INSTALL_PATH}"
    - echo "PV_PLUGIN_PATH= ${PV_PLUGIN_PATH}"
    - echo "PYTHONPATH= ${PYTHONPATH}"
  script:
    - cd build_themysserverplugins_release
    - cmake -DCMAKE_INSTALL_PREFIX=${THEMYSSERVERPLUGINS_INSTALL_PATH} .
    - cmake --install .
    - xvfb-run /opt/paraview_install/bin/pvpython ${PARAVIEW_DATA_ROOT}/ThemysFilters/Testing/WithParaView/pluginspython_ceareaderdat.py
    - xvfb-run /opt/paraview_install/bin/pvpython ${PARAVIEW_DATA_ROOT}/ThemysFilters/Testing/WithParaView/pluginspython_ceawriterdat_vtkmultiblockdataset.py

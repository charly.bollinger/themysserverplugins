/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "vtkMaterialInterface_InterfaceData.h"

#include <vtkDoubleArray.h>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
double InterfaceData::getDistanceInCell(const vtkIdType& cell_index) const
{
  if (m_distance == nullptr)
  {
    return 0.;
  }

  return *m_distance->GetTuple(cell_index);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
InterfaceNormale
InterfaceData::getNormaleInCell(const vtkIdType& cell_index) const
{
  if (m_normale == nullptr)
  {
    return {0., 0., 0.};
  }

  auto* normale_v{m_normale->GetTuple(cell_index)};
  // No choice for pointer arithmetic here as the array is returned from Hercule
  // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  return {*normale_v, *(normale_v + 1), *(normale_v + 2)};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const InterfaceData& if_data)
{
  out << "{Normale: " << if_data.m_normale
      << ", Distance: " << if_data.m_distance << "}";
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
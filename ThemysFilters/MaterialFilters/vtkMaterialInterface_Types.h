/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_Types.h
 * @brief Defines some types aliases
 * @date 2023-01-27
 * 
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 * 
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_TYPES_H
#define VTK_MATERIAL_INTERFACE_TYPES_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <array>
#include <map>
#include <tuple>
#include <utility>

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkSmartPointer.h>
#include <vtkType.h>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/// @brief Define a type alias for 3d coordinates vector
using Vec3 = std::array<double, 3>;

/// @brief Define a type alias to better identify the global cell ids.
/// Global cell ids are shared among cells of different materials (a material is
/// an Unstructured mesh) to identify cells that are superposed (thus they are
/// mixed)
using GlobalIdType = vtkIdType;

/// @brief Define a type alias to better identify the local cell ids.
/// Local cell ids is the vtk id of a cell inside its own material mesh
using LocalIdType = vtkIdType;

/// @brief Define a type alias to better identify the order values
using OrderType = int;

/// @brief Define a type alias to better identify the material id values
using MaterialIdType = int;

/// @brief A ZoneType is an association between a material (identified by its
/// id) and a cell (identified by its local id)
using ZoneType = std::pair<MaterialIdType, LocalIdType>;

/// @brief The collection of zones is a map
using ZonesCollectionType = std::map<OrderType, ZoneType>;

/// @brief The mixed cells are localized inside all cells sharing the same
/// global ids
using MixedCellsLocalizationType = std::map<GlobalIdType, ZonesCollectionType>;

using DataForCellBuildType =
    std::tuple<vtkSmartPointer<vtkPoints>, vtkSmartPointer<vtkCellArray>,
               vtkSmartPointer<vtkPointData>, vtkSmartPointer<vtkCellData>>;
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // VTK_MATERIAL_INTERFACE_TYPES_H
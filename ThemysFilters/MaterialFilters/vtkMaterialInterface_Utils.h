/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_Utils.h
 * @brief Declares some free functions that are used in the vtkMaterialInterface
 * filter
 * @date 2023-01-27
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_UTILS_H
#define VTK_MATERIAL_INTERFACE_UTILS_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <fwd>
#include <string_view> // IWYU pragma: keep
#include <vector>

#include <vtkSmartPointer.h>
#include <vtkType.h>

#include "vtkMaterialInterface_Types.h"

class vtkCell;
class vtkDataSet;
class vtkDoubleArray;
class InterfaceNormale;

/*----------------------------------------------------------------------------*/
/**
 * @brief Return true if the name is empty or equal to None
 *
 * @param name : the name to check
 * @return true
 * @return false
 */
/*----------------------------------------------------------------------------*/
bool is_empty_name(std::string_view name);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns a collection of 3D coordinates of the nodes belonging to
 * the cell in argument
 *
 * @tparam T : input mesh type (vtkDataSet or vtkPoints)
 * @param input_mesh : input mesh
 * @param cell : cell from which the nodes coordinates are looked for
 * @return std::vector<Vec3>
 */
/*----------------------------------------------------------------------------*/
template <class T>
std::vector<Vec3> extract_nodes_coordinates(T* input_mesh, vtkCell* cell);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns a fresh new unstructured grid or a polydata that has been
 * allocated and with points that have been also allocated
 *
 * @tparam T : vtkUnstructuredGrid or vtkPolyData
 * @param estimated_size : size used for allocation
 * @param i_mesh: input mesh from which the cell and point data will be copied
 * @return vtkSmartPointer<T>
 */
/*----------------------------------------------------------------------------*/
template <class T> vtkSmartPointer<T> build_output_mesh(vtkDataSet* i_mesh);

/*----------------------------------------------------------------------------*/
/**
 * @brief Compute the distance of each points described by their coordinates to
 * the plan defined by the normale and distance. The result is stored in the
 * destination array.
 * With outgoing normal, the value obtained is positive if the point is outside
 * the cell (part that has to be kept after the cut) and negative otherwise. The
 * opposite option takes into account the opposite plane by inverting the
 * result.
 *
 * @param pts_coords : coordinates of the points
 * @param destination : array to fill in with the computed distance
 * @param _normal : normale of the plan
 * @param _distance : distance of the plan to the origin
 * @param _opposite : invert the result
 */
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkDoubleArray>
compute_distance_to_plan(const std::vector<Vec3>& pts_coords,
                         const InterfaceNormale& _normal,
                         const double _distance, bool _opposite);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the multiple of 1024 the closest (and less than) to the number
 * of items in argument
 *
 * @param nb_items : number of items the size is estimated for
 * @return vtkIdType
 */
/*----------------------------------------------------------------------------*/
vtkIdType size_estimation(vtkIdType nb_items);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "vtkMaterialInterface_Utils-Impl.h" // IWYU pragma: keep

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // VTK_MATERIAL_INTERFACE_UTILS_H

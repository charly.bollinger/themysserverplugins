vtk_module_add_module(SelectionCellLabel
  CLASSES vtkSelectionCellLabelFilter)

paraview_add_server_manager_xmls(
  XMLS SelectionCellLabel.xml)

"""
CEA DAT Python writer for vtkMultiBlockDataSet, vtkPolyData and
vtkUnstructuredGrid objects

This writer loops over each block, applies the vtkDataSetSurfaceFilter,
applies the FeatureEdge filter if polys exist in the PolyData object,
then applies the vtkReductionFilter, and ultimately saves the coordinates of 
the PolyData nodes obtained. 
"""
from inspect import currentframe, getouterframes
import warnings
from typing import Type

from vtkmodules.vtkCommonDataModel import (
    vtkDataObject,
    vtkCompositeDataSet,
    vtkDataSet,
    vtkPolyData,
    vtkCell,
)
from vtkmodules.vtkCommonExecutionModel import vtkAlgorithm
from vtkmodules.vtkFiltersGeometry import vtkDataSetSurfaceFilter
from vtkmodules.vtkFiltersCore import vtkFeatureEdges
from vtkmodules.vtkCommonCore import vtkLogger
from paraview.modules.vtkPVVTKExtensionsMisc import vtkReductionFilter
from paraview.util.vtkAlgorithm import (
    VTKPythonAlgorithmBase,
    smproxy,
    smproperty,
    smdomain,
)


def VtkLog(message: str) -> None:
    """
    Logs the message with INFO verbosity level

    :param message: message to log
    """
    caller_frame = getouterframes(currentframe())[1]
    qualified_message = f"{caller_frame.function}: {message}"
    vtkLogger.Log(
        vtkLogger.VERBOSITY_INFO,
        caller_frame.filename,
        caller_frame.lineno,
        qualified_message,
    )


def ApplyVtkAlgorithm(obj: vtkDataObject, algo_class: Type[vtkAlgorithm]):
    """
    Applies the algorithm, setting the object as input data, and returns
    the object obtained

    :param obj: object that is used as an input of the algorithm
    :param algo_class: algorithm class to apply
    :return: the object obtained
    :note: if the object in argument is a CompositeDataSet then the object
    returned is also a CompositeDataSet
    see https://docs.paraview.org/en/latest/ReferenceManual/vtkNumPyIntegration.html#handling-composite-datasets
    """
    algo = algo_class()
    algo.SetInputData(obj)
    algo.Update()
    return algo.GetOutputDataObject(0)


def NodeToDatStr(i_node: int, cell: vtkCell, vtkpd: vtkPolyData):
    """
    Writes the coordinates of the node into a string

    :param i_node: node index
    :param cell: the cell the node belongs to
    :param vtkpd: polydata object the cell belongs to
    :return: the string holding coordinates
    """
    i_point = cell.GetPointId(i_node)
    p_coords = vtkpd.GetPoint(i_point)
    try:
        return f"{p_coords[0]:10.10f} {p_coords[1]:10.10f} {p_coords[2]:10.10f}\n"
    except IndexError:
        return f"{p_coords[0]:10.10f} {p_coords[1]:10.10f}\n"


def CellToDatStr(i_cell: int, vtkpd: vtkPolyData) -> str:
    """
    Writes the coordinates of each point defining the cell into a string

    :param i_cell: cell index
    :param vtkpd: polydata object the cell belongs to
    :return: the string holding coordinates
    """
    cell = vtkpd.GetCell(i_cell)
    nb_nodes = cell.GetNumberOfPoints()
    if nb_nodes <= 1:
        return ""

    return "".join(NodeToDatStr(i_node, cell, vtkpd) for i_node in range(nb_nodes))


def PolyDataToDatStr(vtkpd: vtkPolyData, cell_separator="&\n") -> str:
    """
    Writes the coordinates of each point defining a cell in the polydata in argument
    in a string

    :param vtkpd: polydata object to write
    :param cell_separator: string that separates each cell
    :return: the string holding points coordinates
    """
    return cell_separator.join(
        CellToDatStr(i_cell, vtkpd) for i_cell in range(vtkpd.GetNumberOfCells())
    )


def AppendPolyDataToContourFile(
    filename: str, vtkpd: vtkPolyData, polydata_sep="&\n"
) -> None:
    """
    Append the coordinates of each nodes of vtk poly data into the file in argument

    :param filename: path to the file to write
    :param vtkpd: the polydata to write
    :param polydata_sep: the string that separates each polydata
    """
    with open(filename, "a") as fd_out:
        VtkLog(f"Writing polydata to {filename}")
        data = PolyDataToDatStr(vtkpd) + polydata_sep
        fd_out.write(data)


@smproxy.writer(
    extensions="dat", file_description="CEA Writer DAT (Python)", support_reload=False
)
@smproperty.input(name="Input", port_index=0)
@smdomain.datatype(dataTypes=["vtkDataSet"], composite_data_supported=True)
class CEAWriterDAT_vtkMultiBlockDataSet(VTKPythonAlgorithmBase):
    """
    This class extracts the contour of a dataset and writes it to a file under
    dat format

    Strongly inspired from:
    https://gitlab.kitware.com/paraview/paraview/-/snippets/425
    """

    def __init__(self):
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=1, nOutputPorts=0, inputType="vtkDataSet"
        )
        self._version = "Writer DAT (vtkMultiBlockDataSet) 2.0 (c)JBL&GP 2022"
        self._filename = None
        vtkLogger.Init()
        vtkLogger.SetStderrVerbosity(vtkLogger.VERBOSITY_INFO)

    def FillInputPortInformation(self, port, info):
        """Allows us to save composite datasets as well."""
        info.Set(self.INPUT_REQUIRED_DATA_TYPE(), self.InputType)
        info.Append(self.INPUT_REQUIRED_DATA_TYPE(), "vtkCompositeDataSet")
        return 1

    @smproperty.stringvector(name="FileName", panel_visibility="never")
    @smdomain.filelist()
    def SetFileName(self, _filename):
        """Specify filename for the file to write."""
        if self._filename != _filename:
            self._filename = _filename
            self.Modified()

    @smproperty.stringvector(name="GetVersion", information_only="1")
    def GetVersion(self):
        return self._version

    def ExtractContour(self, inputDataObject: vtkDataSet):
        """
        Applies different filters to extract the object contour and writes it
        down into the output file
        """
        # The DataSetSurfaceFilter extract the surface.
        # In 3D we obtain a 2D surface, ie with cells on it, thats why the
        # call to FeatureEdge is necessary to get back to 1D structure.
        # In 2D this call is certainly useless but for the sake of simplicity
        # it remains as is.
        # In all cases, the return object is a vtkPolyData!
        VtkLog("Applying the vtkDataSetSurfaceFilter")
        surface_pd: vtkPolyData = ApplyVtkAlgorithm(
            inputDataObject, vtkDataSetSurfaceFilter
        )

        if surface_pd.GetNumberOfPolys():
            # Extract the Edges. As the DataSetSurfaceFilter ensures the structure
            # at this point is 2D, then the structure after the FeatureEdge will be
            # 1D.
            # Do not do this if the data does not have polys other wise the number
            # of cells obtained will be null
            VtkLog("Applying the vtkFeatureEdges filter")
            surface_pd = ApplyVtkAlgorithm(surface_pd, vtkFeatureEdges)

        # Apply parallelism reduction in order to write one file
        VtkLog("Applying the vtkReductionFilter")
        surface_pd = ApplyVtkAlgorithm(surface_pd, vtkReductionFilter)

        # Remove ghost cells to avoid overlapping points in the output file
        VtkLog("Removing ghost cells")
        surface_pd.RemoveGhostCells()

        AppendPolyDataToContourFile(self._filename, surface_pd)

    def WalkThroughCompositeDataSetTree(self, inputDataObject: vtkCompositeDataSet):
        """
        Recurse over the composite data set tree to ultimately calls the
        treatment method over the leafs of the tree
        """
        # Wrap the input data object just to iterate over
        cds_iter = inputDataObject.NewIterator()
        while not cds_iter.IsDoneWithTraversal():
            dataset = inputDataObject.GetDataSet(cds_iter)
            if dataset.IsTypeOf(self.InputType):
                VtkLog(
                    f"A leaf ({dataset.GetClassName()}) "
                    " of the composite data set is reached"
                )
                # Here self.InputType is vtkDataSet i.e the leaf of
                # the CompositeDataSet tree is reached (base case recursion)
                self.ExtractContour(dataset)
            elif dataset.IsTypeOf("vtkCompositeDataSet"):
                VtkLog(f"Dealing with a composite data set ({dataset.GetClassName()}) ")
                # Here we handle the recursive case, i.e when, for example,
                # a MultiBlockDataSet is made of other MultiBlockDataSets or
                # MultiPieceDataSet
                self.WalkThroughCompositeDataSetTree(dataset)
            else:
                warnings.warn(
                    "Input block %d of type(%s) not saveable by writer."
                    % (dataset.GetCurrentFlatIndex(), type(dataset))
                )
            cds_iter.GoToNextItem()

    def RequestData(self, request, inInfoVec, outInfoVec):
        """
        Determines the composite aspect of the input and triggers recursion
        accordingly
        """
        inp = self.GetInputData(inInfoVec, 0, 0)
        if isinstance(inp, vtkCompositeDataSet):
            VtkLog(f"Dealing with a composite data set ({inp.GetClassName()})")
            self.WalkThroughCompositeDataSetTree(inp)
        else:
            VtkLog(f"Dealing with a dataset ({inp.GetClassName()})")
            self.ExtractContour(inp)
        return 1

    def Write(self):
        self.Modified()
        self.Update()

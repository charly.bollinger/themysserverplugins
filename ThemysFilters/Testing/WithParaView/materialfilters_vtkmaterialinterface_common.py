# This module holds common functions used by python tests of the 
# single cell type

# paraview version 5.10.0-1598-g39b552c538
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 10

from typing import TypeVar, Callable
from pathlib import Path
from typing import TypeVar
import sys

from paraview.simple import *
from paraview.vtk.test import Testing

from common import CMAKE_SOURCE_DIR

RenderView = TypeVar('RenderView')
MaterialInterfaceFilterType = TypeVar("MaterialInterfaceLOVE")
InterfaceFilterManipulatorType = Callable[[MaterialInterfaceFilterType], None]
DataBaseManipulatorType = Callable[[XMLMultiBlockDataReader], None]
InterfaceDisplayType = TypeVar('materialInterfaceLOVE1Display')
InterfaceDisplayManipulatorType = Callable[[InterfaceDisplayType], None]


def load_data_and_apply_filter(database_path: str, database_name: str, selected_block: str,
                               interface_filter_configurator: InterfaceFilterManipulatorType=None,
                               interface_display_configurator: InterfaceDisplayManipulatorType=None
                               ) -> RenderView:
    """
    Load the database in argument and display the block

    :param database_path: path to the database starting from CMAKE_SOURCE_DIR directory
    :param database_name: name of the database
    :param selected_block: block to display
    :param interface_filter_configurator: functor that configure the interface filter
    :return: the render view, the reader source and the material interface filter
    """
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    # create a new 'XML MultiBlock Data Reader'
    database_vtm = XMLMultiBlockDataReader(registrationName=database_name, FileName=[f'{CMAKE_SOURCE_DIR}/{database_path}'])

    # get active view
    render_view = GetActiveViewOrCreate('RenderView')

    # show data in view
    Show(database_vtm, render_view, 'UnstructuredGridRepresentation')

    # create a new 'Material Interface (LOVE)'
    materialInterfaceLOVE1 = MaterialInterfaceLOVE(registrationName='MaterialInterfaceLOVE1', Input=database_vtm)
    materialInterfaceLOVE1.MaskArray = ['POINTS', 'None']
    materialInterfaceLOVE1.NormalArray2 = ['POINTS', 'None']
    materialInterfaceLOVE1.DistanceArray2 = ['POINTS', 'None']
    if interface_filter_configurator is not None:
        interface_filter_configurator(materialInterfaceLOVE1)

    # show data in view
    materialInterfaceLOVE1Display = Show(materialInterfaceLOVE1, render_view, 'UnstructuredGridRepresentation')

    # trace defaults for the display properties.
    materialInterfaceLOVE1Display.Representation = 'Surface With Edges'
    materialInterfaceLOVE1Display.ColorArrayName = [None, '']
    materialInterfaceLOVE1Display.SelectTCoordArray = 'None'
    materialInterfaceLOVE1Display.SelectNormalArray = 'None'
    materialInterfaceLOVE1Display.SelectTangentArray = 'None'
    materialInterfaceLOVE1Display.OSPRayScaleFunction = 'PiecewiseFunction'
    materialInterfaceLOVE1Display.SelectOrientationVectors = 'None'
    materialInterfaceLOVE1Display.ScaleFactor = 0.0020000010728836062
    materialInterfaceLOVE1Display.SelectScaleArray = 'None'
    materialInterfaceLOVE1Display.GlyphType = 'Arrow'
    materialInterfaceLOVE1Display.GlyphTableIndexArray = 'None'
    materialInterfaceLOVE1Display.GaussianRadius = 0.0001000000536441803
    materialInterfaceLOVE1Display.SetScaleArray = [None, '']
    materialInterfaceLOVE1Display.ScaleTransferFunction = 'PiecewiseFunction'
    materialInterfaceLOVE1Display.OpacityArray = [None, '']
    materialInterfaceLOVE1Display.OpacityTransferFunction = 'PiecewiseFunction'
    materialInterfaceLOVE1Display.DataAxesGrid = 'GridAxesRepresentation'
    materialInterfaceLOVE1Display.PolarAxes = 'PolarAxesRepresentation'
    materialInterfaceLOVE1Display.ScalarOpacityUnitDistance = 0.022449253008898756
    materialInterfaceLOVE1Display.OpacityArrayName = ['CELLS', 'Milieu:Density']
    materialInterfaceLOVE1Display.SelectInputVectors = [None, '']
    materialInterfaceLOVE1Display.WriteLog = ''

    if (interface_display_configurator is not None):
        interface_display_configurator(materialInterfaceLOVE1Display)

    # hide data in view
    Hide(database_vtm, render_view)

    # update the view to ensure updated data information
    render_view.Update()

    # set scalar coloring
    ColorBy(materialInterfaceLOVE1Display, ('FIELD', 'vtkBlockColors'))

    # show color bar/color legend
    materialInterfaceLOVE1Display.SetScalarBarVisibility(render_view, True)

    # Properties modified on materialInterfaceLOVE1Display
    materialInterfaceLOVE1Display.BlockSelectors = []

    # set active source
    SetActiveSource(materialInterfaceLOVE1)

    # Properties modified on materialInterfaceLOVE1Display
    if selected_block is not None:
        materialInterfaceLOVE1Display.BlockSelectors = [f'/Root/M1/{selected_block}']

    return render_view, database_vtm, materialInterfaceLOVE1


def set_camera_up(render_view: RenderView, camera_position: list[float], camera_focal_point: list[float], camera_parallel_scale: float):
    """
    Sets the camera position, focal point and parallel scale

    :param render_view: the render view
    :param camera_position: the camera position
    :param camera_focal_point: the camera focal point
    :param camera_parallel_scale: the camera parallel scale
    """
    # get layout
    layout1 = GetLayout()

    #--------------------------------
    # saving layout sizes for layouts

    # layout/tab size in pixels
    layout1.SetSize(455, 530)

    #-----------------------------------
    # saving camera placements for views

    # current camera placement for renderView1
    render_view.InteractionMode = '2D'
    render_view.CameraPosition = camera_position 
    render_view.CameraFocalPoint = camera_focal_point
    render_view.CameraParallelScale = camera_parallel_scale


def launch_comparison(render_view: RenderView, baseline_image_suffix=""):
    """
    Launch the vtk testing comparison process

    :param render_view: the render view
    """
    this_file = Path(sys.argv[0]).resolve()
    baseline_path = Path(f"{CMAKE_SOURCE_DIR}") / "Data" / "Baseline"
    this_file_name = this_file.stem
    baseline_file = baseline_path / (this_file_name + baseline_image_suffix + ".png")
    Testing.compareImage(render_view.GetRenderWindow(), baseline_file.as_posix(), threshold=0)
    Testing.interact()


def run(database_path: str, database_name: str, selected_block: str, camera_position: list[float],
        camera_focal_point: list[float], camera_parallel_scale: float,
        interface_filter_configurator: InterfaceFilterManipulatorType = None,
        interface_display_configurator: InterfaceDisplayManipulatorType = None,
        database_asserter: DataBaseManipulatorType = None,
        interface_filter_asserter: InterfaceFilterManipulatorType = None
        ):
    """
    Run the test

    :param database_path: path to the database starting from CMAKE_SOURCE_DIR directory
    :param database_name: name of the database
    :param selected_block: block to display
    :param camera_position: the camera position
    :param camera_focal_point: the camera focal point
    :param camera_parallel_scale: the camera parallel scale
    :param interface_filter_configurator: functor that configure the interface filter
    """
    renderView1, database, filter = load_data_and_apply_filter(database_path, database_name, selected_block, interface_filter_configurator, interface_display_configurator)
    if database_asserter:
        database_asserter(database)
    if interface_filter_asserter:
        interface_filter_asserter(filter)
    set_camera_up(renderView1, camera_position, camera_focal_point, camera_parallel_scale)
    launch_comparison(renderView1)

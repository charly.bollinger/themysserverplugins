
"""
This test checks the vtkMaterialInterface filter behavior, using global algorithm
and with contour output (FillMaterial=Off).
This is a special situation, because the three required interface arrays are missing 
from the database. This is the case for example in client/server mode when one of
the server doesn't hold any mixed cell (so any interface).
In this case the filter does not pass the mesh in input to the output
otherwise the whole pvserver domain will be appear (with cells and data on it) to the 
side of the interface lines.
It consist in just one image comparison because the image is empty.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *

from common import CMAKE_SOURCE_DIR
from materialfilters_vtkmaterialinterface_common import launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML MultiBlock Data Reader'
hDepnTemps_usp0001_000005vtm = XMLMultiBlockDataReader(
    registrationName="HDep-n=Temps_u=s.p-0001_000005.vtm",
    FileName=[
        f"{CMAKE_SOURCE_DIR}/Data/Testing/Interfaces/4MatsVortex_WrongData/4mats_vortex_empty_interfaces.vtm"
    ],
)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
hDepnTemps_usp0001_000005vtmDisplay = Show(
    hDepnTemps_usp0001_000005vtm, renderView1, "GeometryRepresentation"
)

# reset view to fit data
renderView1.ResetCamera(False)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Material Interface (LOVE)'
materialInterfaceLOVE1 = MaterialInterfaceLOVE(
    registrationName="MaterialInterfaceLOVE1", Input=hDepnTemps_usp0001_000005vtm
)
materialInterfaceLOVE1.MaskArray = ["POINTS", "None"]
materialInterfaceLOVE1.NormalArray2 = ["POINTS", "None"]
materialInterfaceLOVE1.DistanceArray2 = ["POINTS", "None"]
materialInterfaceLOVE1.FillMaterialOn = 0
materialInterfaceLOVE1.NormalArray = "vtkInterfaceNormal"
materialInterfaceLOVE1.DistanceArray = "vtkInterfaceDistance"
materialInterfaceLOVE1.OrderArray = "vtkInterfaceOrder"

# show data in view
materialInterfaceLOVE1Display = Show(
    materialInterfaceLOVE1, renderView1, "GeometryRepresentation"
)

# trace defaults for the display properties.
materialInterfaceLOVE1Display.Representation = "Surface"
materialInterfaceLOVE1Display.ColorArrayName = [None, ""]
materialInterfaceLOVE1Display.SelectTCoordArray = "None"
materialInterfaceLOVE1Display.SelectNormalArray = "None"
materialInterfaceLOVE1Display.SelectTangentArray = "None"
materialInterfaceLOVE1Display.OSPRayScaleFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.SelectOrientationVectors = "None"
materialInterfaceLOVE1Display.ScaleFactor = 0.1
materialInterfaceLOVE1Display.SelectScaleArray = "None"
materialInterfaceLOVE1Display.GlyphType = "Arrow"
materialInterfaceLOVE1Display.GlyphTableIndexArray = "None"
materialInterfaceLOVE1Display.GaussianRadius = 0.005
materialInterfaceLOVE1Display.SetScaleArray = [None, ""]
materialInterfaceLOVE1Display.ScaleTransferFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.OpacityArray = [None, ""]
materialInterfaceLOVE1Display.OpacityTransferFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.DataAxesGrid = "GridAxesRepresentation"
materialInterfaceLOVE1Display.PolarAxes = "PolarAxesRepresentation"
materialInterfaceLOVE1Display.SelectInputVectors = [None, ""]
materialInterfaceLOVE1Display.WriteLog = ""

# hide data in view
Hide(hDepnTemps_usp0001_000005vtm, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(materialInterfaceLOVE1Display, ("FIELD", "vtkBlockColors"))

# show color bar/color legend
materialInterfaceLOVE1Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.PointSize = 5.0

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.LineWidth = 5.0

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.BlockSelectors = [
    "/Root/M1/_1NE",
    "/Root/M1/_2NW",
    "/Root/M1/_3SE",
    "/Root/M1/_4SW",
]

# get layout
layout1 = GetLayout()

# layout/tab size in pixels
layout1.SetSize(1482, 867)

# current camera placement for renderView1
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView1.CameraParallelScale = 0.7071067811865476

launch_comparison(renderView1)
